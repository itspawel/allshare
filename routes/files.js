const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const File = require("../models/file");
const { v4: uuid4 } = require("uuid");

let storage = multer.diskStorage({
    destination: (req, file, cb) => cb(null, "uploads/"),

    filename: (req, file, cb) => {
        const uniqueName = `allShare-2021${Date.now()}${path.extname(
            file.originalname
        )}`;
        cb(null, uniqueName);
    },
});

let upload = multer({
    storage,
    limits: { fileSize: 100000 * 100 },
}).single("myfile"); //Use the name attribute from the HTML form

router.post("/", (req, res) => {
    upload(req, res, async (err) => {
        // if (!req.file) {
        //     return res.json({ error: "All fields are required." });
        // }

        if (err) {
            return res.status(500).send({ error: err.message });
        }

        //Store into DB

        const file = new File({
            filename: req.file.filename,
            uuid: uuid4(),
            path: req.file.path,
            size: req.file.size,
        });

        const response = await file.save();
        res.json({
            file: `${process.env.APP_BASE_URL}files/${response.uuid}`,
        });

        //http://localhost:3000/files/allShare-20210404182790.png
    });

    router.post("/send", async (req, res) => {
        const { uuid, emailTo, emailFrom, expiresIn } = req.body;

        //Validate request
        if (!uuid || !emailTo || !emailFrom) {
            return res.status(422).send({ error: "All fields are required" });
        }

        //Get data from DB

        try {
            const file = await File.findOne({ uuid: uuid });
            if (file.sender) {
                return res.status(422).send({ error: "Email already sent." });
            }
            file.sender = emailFrom;
            file.receiver = emailTo;
    
            const response = await file.save();
    
            // Send the email
            const sendMail = require("../services/emailService");
            sendMail({
                from: emailFrom,
                to: emailTo,
                subject: "allShare File Sharing",
                text: `${emailFrom} shared a file with you.`,
                html: require("../services/emailTemplate")({
                    emailFrom: emailFrom,
                    downloadLink: `${process.env.APP_BASE_URL}/files/${file.uuid}`,
                    size: parseInt(file.size / 1000) + "KB",
                    expires: "24 hours",
                }),
            }).then(()=>{
                return res.send({ success: true });
            }).catch(err=>{
                return res.status(500).json({error: 'Error in sending email.'});
            });
        } catch(err){
            return res.status(500).send({error: 'Something went wrong.'});
        }
    })
});

module.exports = router;
